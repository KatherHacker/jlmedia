//
//  SplashViewController.swift
//  JLMedia
//
//  Created by KatherHussain on 09/08/21.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var logoCenterXConstraint : NSLayoutConstraint!
    @IBOutlet weak var bottomLblBottomConstraint : NSLayoutConstraint!
    @IBOutlet weak var colorViewTopConstraint : NSLayoutConstraint!
    
    @IBOutlet weak var logoImage : UIImageView!
    @IBOutlet weak var bottomLbl : UILabel!
    @IBOutlet weak var colorView : UIView!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        setupAnimationConstraints()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        setupAnimationConstraints()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        splashAnimation()
    }
    
    func splashAnimation(){
        UIView.animate(withDuration: 3) {
            self.logoCenterXConstraint?.constant = 0
            self.bottomLblBottomConstraint?.constant = 60
            self.colorViewTopConstraint?.constant = 0
            self.view.layoutIfNeeded()
        } completion: { (state) in
            let vc = Router.main.instantiateViewController(withIdentifier: "MediaPlayerViewController") as? MediaPlayerViewController
            
            self.present(vc!, animated: true, completion: nil)
        }

    }
    
    func setupAnimationConstraints() {
        self.logoCenterXConstraint?.constant = -((UIScreen.main.bounds.width/2) + (self.logoImage.bounds.width/2))
        self.bottomLblBottomConstraint?.constant = -((self.bottomLbl.bounds.height) + (60))
        self.colorViewTopConstraint?.constant = -(self.colorView.bounds.height)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
